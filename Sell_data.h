#ifndef SELL_DATA_H_INCLUDED
#define SELL_DATA_H_INCLUDED
typedef struct               //下单日期
{
	int year;
	int month;
	int day;
} OrderDate;
typedef struct               //运输日期
{
	int post_year;
	int post_month;
	int post_day;
} PostDate;
    typedef struct            //顾客信息
{
    char Code[20];           //顾客代码
    char Name[20];           //姓名
    int  vip;                //顾客名称
    char Address[30];        //地址
    char PhoneNumber[20];    //电话
    char memo[50];           //备注
}Customer;
typedef struct               //订单信息
{
    char OrderNumber[20];    //订单号
    OrderDate;               //下单日期
    char Code[20];           //顾客代码
    char PostRequire[20];    //运输要求
    char VendorLogo[20];     //能否供货标志
    PostDate;                //运输日期
    double SpotWeigth;       //货物重量
    int Freight;             //运费
    char PaymentBill;        //付款清单

}orderInfo;
typedef struct               //订单细则（对订单信息各子项的详细说明）
{
    char RuleNumber[20];     //细则编号
    char OrderNumber[20];    //订单号
    int  ClassNumber[20];    //产品分类编号
    char Production[20];     //生产厂
    int  Quantity;           //数量
    int  TotalAmount;     //总金额
}orderRules;
typedef struct               //库存商品信息
{
    char ClassCode[20];      //商品分类编码
    char ProductCode[20];    //生产厂家编码
    char Description[20];    //商品说明
    int Price;               //单价
}StockInfo;
typedef struct               //生产厂家信息
{
    char PlantCode[20];      //厂家代码
    char plantName[20];      //厂家名称
}Plant;

void welcome();//欢迎界面

int judge();//判断身份用户选择所需平台
//
void customer_menu(); //customer_menu顾客需求选择菜单,返回用户选择功能模块序号
//
void manager_menu();//manager_menu管理者需求选择菜单，返回用户选择功能模块序号
//
/////////////////////////////////////////前台
int order();//订购商品、请求发票，返回商品总金额（函数内需调用void  menu()）
//
int write_info(Customer s[]);//填写邮寄信息（比如收件人、地址、联系电话及订单号等）,并打印让用户确定订单
//
void pay_info(Customer s[],int total,int result);//提供支付信息、总金额，提示支付成功
//
/////////////////////////////////////////后台
int verify_order(orderRules x[]);//确认用户的订单、发货及打印发票（或缺货及缺货处理）
//
int stock_keep(orderRules x[]);//对库存信息维护、库存不足时及时联系生产厂家进货
//
//int show_info(); //生成已成功完成订单列表、未付款订单报表、未发货订单报表
//
/////////////////////////////////////////qita
//int store_info();//来储存顾客发票信息、需要生成的订单列表、未付款订单报表、未发货订单报表viod menu()
void  menu();//店铺菜单

#endif
