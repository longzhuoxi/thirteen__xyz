#include<stdio.h>
#include <stdlib.h>
#include "Sell_data.h"
void pay_info(Customer s[],int total,int result)//提供支付信息、总金额，提示支付成功
{
    int pay1;//用户实际需付款金额
    int pay2;//用户输入的付款金额
    char choose;//用户确定订单是否有误
    char write;//用户反馈订单信息问题
    printf("\n----------------------------即将进入支付界面......\n");
    printf("请确认您的订单信息是否有误\n");
    printf("                    【订单】                 \n");
    printf("|-------------------------------------------|\n");
    printf("|   姓名：%s                                \n",s[1].Name);
    printf("|   是否为VIP用户（Y：是；N：否）:%s        \n",s[1].vip);
    printf("|   收货地址 : %s                           \n",&s[1].Address);
    printf("|   联系电话 ：%s                           \n",s[1].PhoneNumber);
    printf("|   订单备注：%s                            \n",s[1].memo);
    printf("|   订单总金额：%d                          \n",total);
    printf("|   应付款：%d                              \n",total);
    if(total<19)
    {
         pay1=total;
         printf("|   实际付款(会员享有满减优惠)：%d          \n",pay1);
    }
    if(total>=19)//会员满19减2
    {
        pay1=total-result*2;
        printf("|   实际付款(会员享有满减优惠)：%d          \n",pay1);
    }
    if(total>=39)//会员满39减6
    {
        pay1=total-result*6;
        printf("|   实际付款(会员享有满减优惠)：%d          \n",pay1);
    }
    if(total>=69)//会员满69减10
    {
        pay1=total-result*10;
        printf("|   实际付款(会员享有满减优惠)：%d          \n",pay1);
    }
    printf("---------------------------------------------\n");
    printf("请确认订单是否有误（有误:Y;无误：N）\n请输入：");
    scanf("%s",&choose);
    if(choose=='N'||choose=='n')
    {
        printf("您已确定订单无误！请继续完成支付\n");
        while(1)
        {
            printf("请输入实际付款金额：\n");
            scanf("%d",&pay2);
            if(pay2==pay1)
            {
                printf("您已成功支付！我们将尽快派送您的订单......\n");
                printf("美味值得等待，欢迎您的下次光临！\n");
                break;
            }
            else
            {
                printf("您输入的金额有误！请确认后重新输入......\n");
            }
        }

    }
    else
    {
        printf("我们的失误！还请您备注错误的信息项，并写下正确信息\n");
        scanf("%s",&write);
    }
}
